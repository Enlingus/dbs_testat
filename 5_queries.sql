-- Aufgabe 1
--Testat 3 Aufgabe 1.1 und 1.2
\echo Testat 3 Aufgabe 1.1 und 1.2

select distinct 
        per.personid as "Personen-Nummer",
        per.surname as "Vorname", 
        per.name as "Nachname"
    from 
        person per
        inner join customer cus on per.personid = cus.personid 
        inner join reservation res on cus.personid = res.customerid 
    order by
       1;

--Testat 3 Aufgabe 1.3 und 1.4 (korreliert)
\echo Testat 3 Aufgabe 1.3 und 1.4 (korreliert)

select distinct 
        per.personid as "Personnen-Nummer",
        per.surname as "Vorname",
        per.name as "Nachname"
    from
        person per
        inner join customer cus on per.personid  = cus.personid
    where
        cus.personid in (select res.customerid from reservation res where res.customerid = cus.personid and totalprice > 7000)
    order by
        1;

-- Aufgabe 2
-- Testat 3 Aufgabe 2.1 CTE (unkorreliert)
\echo Testat 3 Aufgabe 2.1 CTE (unkorreliert)

\echo ohne CTE!

select  
        per.personid as "Personen-Nummer", 
        per.surname as "Vorname", 
        per.name as "Nachname"
    from 
        person per
    where 
        per.personid in (select personid from customer) 
        and
        per.personid in (select personid from employee)
    order by 
        1;

\echo mit CTE!

with person_in_customer_set(personid) as 
(
    select personid from employee
), person_in_employee_set(personid) as
(
    select personid from customer
)


select  
        per.personid as "Personen-Nummer", 
        per.surname as "Vorname", 
        per.name as "Nachname"
    from 
        person per
    where 
        per.personid in (select * from person_in_customer_set)
        and
        per.personid in (select * from person_in_employee_set)
    order by 
        1;

-- Testat 3 Aufgabe 2.2
\echo Testat 3 Aufgabe 2.2

select
        per.Surname as "Vorname",
        per.Name as "Nachname",
        sum(res.totalprice) as "gesamte Bestellsumme für Kunden"
    from 
        person per
        inner join customer cus on per.personid = cus.personid
        inner join reservation res on cus.personid = res.customerid
    group by
        per.personid
    order by
        3;

select distinct
        framenumber as "Velonummer",
        count(framenumber) over (partition by framenumber) as "Anzahl Ausleihen pro Velonummer"
from 
    reservationposition;

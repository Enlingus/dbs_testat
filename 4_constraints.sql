ALTER TABLE Employee
ADD CONSTRAINT fk_employee
	FOREIGN KEY (PersonID) REFERENCES Person (PersonID)
	ON DELETE CASCADE
;

ALTER TABLE Customer
ADD CONSTRAINT fk_customer
	FOREIGN KEY (PersonID) REFERENCES Person (PersonID)
	ON DELETE CASCADE
;

ALTER TABLE ReservationPosition
ADD CONSTRAINT fk_ReservationNumber
	FOREIGN KEY (ReservationNumber) REFERENCES Reservation (ReservationNumber)
	ON DELETE CASCADE
;

ALTER TABLE ReservationPosition
ADD CONSTRAINT fk_FrameNumber
        FOREIGN KEY (FrameNumber) REFERENCES Bike (FrameNumber)
        ON Delete CASCADE
;

ALTER TABLE Reservation
ADD CONSTRAINT fk_customernumber
        FOREIGN KEY (CustomerID) REFERENCES Customer (PersonID)
        ON Delete CASCADE
;

ALTER TABLE Person 
ADD CONSTRAINT min_age_check
CHECK (extract(year from age(Birthday))::int >17)
;

ALTER TABLE Reservation
ADD CONSTRAINT min_price_check
CHECK (totalprice >= 100)
;
